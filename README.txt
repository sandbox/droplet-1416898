
*******************************************************
    README.txt for field_image_alt_title.info.module
*******************************************************

This module provides a way to change the maxlength of Alt & Title to any value.

Why develop this module ?
Default value of Image Field Alt & Title maxlength are unreasonable.
Read more: http://drupal.org/node/1353030#comment-5506350

FAQ

Why I got error mesage ...... ?
PDOException: SQLSTATE[22001]: String data, right truncated: 1406 Data too long
for column 'field_image_title' at row 1: ALTER TABLE {field_data_field_image}
CHANGE `field_image_title` `field_image_title` VARCHAR(1) NULL DEFAULT NULL; Array
 ( ) in db_change_field() (line 2988 of ......./includes/database/database.inc).


This is because you set a value lower than the date long for column.
Increase you maxlength value.